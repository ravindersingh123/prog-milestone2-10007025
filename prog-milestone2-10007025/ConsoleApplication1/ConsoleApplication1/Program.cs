﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
       
            static void Main(string[] args)
        {
                fooddictionoary();
            }


        public static void fooddictionoary()



        {
            Console.WriteLine("Enter your 5 favourite foods ");
            Console.WriteLine(" ");

            Console.WriteLine("Enter your first food choice");
            var f1 = Console.ReadLine();
            Console.WriteLine("Enter your second food choice");
            var f2 = Console.ReadLine();
            Console.WriteLine("Enter your third food choice");
            var f3 = Console.ReadLine();
            Console.WriteLine("Enter your fourth food choice");
            var f4 = Console.ReadLine();
            Console.WriteLine("Enter your fifth food choice");
            var f5 = Console.ReadLine();
            Console.WriteLine(" ");

            Console.WriteLine($"Please rate {f1} from 1 to 5");
            int ratef1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {f2} from 1 to 5");
            int ratef2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {f3} from 1 to 5");
            int ratef3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {f4} from 1 to 5");
            int ratef4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Please rate {f5} from 1 to 5");
            int ratef5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(" ");

            var favfoods = new Dictionary<string, string>();
            favfoods.Add($"{f1}", $"{ratef1}");
            favfoods.Add($"{f2}", $"{ratef2}");
            favfoods.Add($"{f3}", $"{ratef3}");
            favfoods.Add($"{f4}", $"{ratef4}");
            favfoods.Add($"{f5}", $"{ratef5}");

            if (f1 == string.Empty)
            {
                Console.WriteLine($"You failed to enter favourite foods. Please restart");
                Console.WriteLine("enter the key to close the program");
                Console.ReadKey();
                return;
            }
            else

            if (f2 == string.Empty)
            {
                Console.WriteLine($"You failed to enter favourite foods. please restart");
                Console.WriteLine("enter the key to close the program");
                Console.ReadKey();
                return;
            }
            else

            if (f3 == string.Empty)
            {
                Console.WriteLine($"You failed to enter favourite foods. Please restart");
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else
            {
                //do nothing    
            }
            if (f4 == string.Empty)
            {
                Console.WriteLine($"You failed to enter favourite foods. Please restart");
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else

            if (f5 == string.Empty)
            {
                Console.WriteLine($"You failed to enter favourite foods. Please restart");
                Console.WriteLine("Press any key to close the program");
                Console.ReadKey();
                return;
            }
            else

                Console.WriteLine("Do you like to see your rated foods? enter Y/N");
            Console.WriteLine(" ");

            var displayfoods = Console.ReadLine();
            switch (displayfoods)
            {
                case "Y":
                    foreach (var item in favfoods.OrderBy(Key => Key.Value))

                        Console.WriteLine($"Your rating for {item.Key} was {item.Value}");
                    Console.WriteLine(" ");
                    break;
                case "N":
                    break;
                default:
                    Console.WriteLine("wrong input. Food rating is not available");
                    break;
            }
        }
    }
}
       
